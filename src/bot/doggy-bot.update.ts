import { Action, Ctx, Start, Update } from 'nestjs-telegraf';
import { Context } from 'telegraf';
import { SubscriptionManagerService } from './subscription-manager.service';
import { Logger } from '@nestjs/common';

@Update()
export class DoggyBotUpdate {
    private log = new Logger(DoggyBotUpdate.name);

    constructor(private subcrManager: SubscriptionManagerService) {}

    @Start()
    async onStart(@Ctx() ctx: Context): Promise<void> {
        await ctx.reply('Привет!', {
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: 'Подписаться на уведомление',
                            callback_data: 'sb',
                        },
                    ],
                ],
            },
        });
    }

    @Action(['sb'])
    async onSubscribe(@Ctx() ctx: Context): Promise<void> {
        this.subcrManager.subscribe(ctx);
        await ctx.editMessageText('Ты подписан на уведомления!');
    }

    @Action(['unsb'])
    async onUnsubscribe(@Ctx() ctx: Context): Promise<void> {
        this.subcrManager.unsubscribe(ctx);
        await ctx.reply('Ок! Больше присылать не буду.', {
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: 'Подписаться на уведомление',
                            callback_data: 'sb',
                        },
                    ],
                ],
            },
        });
    }

    @Action(['accept'])
    async onAcceptFeed(@Ctx() ctx: Context): Promise<void> {
        const messageId = ctx.update['callback_query'].message.message_id;
        await ctx.deleteMessage(messageId);
        this.log.log(`Message with id ${messageId} has been deleted`);
    }
}
