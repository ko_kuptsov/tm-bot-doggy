import { Injectable, Logger } from '@nestjs/common';
import { Context, Telegraf } from 'telegraf';
import { FeedDogCronService } from '../common/feed-dog-cron.service';
import { InjectBot } from 'nestjs-telegraf';

@Injectable()
export class SubscriptionManagerService {
    private log = new Logger(SubscriptionManagerService.name);
    private subscribersChatId = new Set<string>([]);

    constructor(
        private feedDailyCronService: FeedDogCronService,
        @InjectBot() private bot: Telegraf<Context>,
    ) {
        this.feedDailyCronService.subscribe(() => {
            this.subscribersChatId.forEach((chatId) => {
                try {
                    this.bot.telegram.sendMessage(chatId, '*ПОКОРМИ МЕНЯ*', {
                        parse_mode: 'MarkdownV2',
                        reply_markup: {
                            inline_keyboard: [
                                [
                                    {
                                        text: 'Покормил!',
                                        callback_data: 'accept',
                                    },
                                    {
                                        text: 'Отписаться',
                                        callback_data: 'unsb',
                                    },
                                ],
                            ],
                        },
                    });
                } catch (err) {
                    this.log.error(
                        `Got an error while sending message to chatId - ${chatId}`,
                        err,
                    );
                }
            });
        });
    }

    subscribe(ctx: Context): void {
        const chatId = ctx.update['callback_query'].message.chat.id;
        this.subscribersChatId.add(chatId);
        this.log.log(`Added new subscriber with chatId ${chatId}`);
    }

    unsubscribe(ctx: Context): void {
        const chatId = ctx.update['callback_query'].message.chat.id;
        this.subscribersChatId.delete(chatId);
        this.log.log(`Subscriber with chatId ${chatId} has been removed`);
    }
}
