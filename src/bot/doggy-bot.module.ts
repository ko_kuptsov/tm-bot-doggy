import { Module } from '@nestjs/common';
import { TelegrafModule } from 'nestjs-telegraf';
import { ConfigService } from '@nestjs/config';
import { DoggyBotUpdate } from './doggy-bot.update';
import { SubscriptionManagerService } from './subscription-manager.service';
import { FeedDogCronService } from '../common/feed-dog-cron.service';

@Module({
    imports: [
        TelegrafModule.forRootAsync({
            useFactory: async (configService: ConfigService) => ({
                token: configService.get<string>('TELEGRAM_BOT_TOKEN'),
            }),
            inject: [ConfigService],
        }),
    ],
    providers: [DoggyBotUpdate, FeedDogCronService, SubscriptionManagerService],
})
export class DoggyBotModule {}
