import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { Subject } from 'rxjs';

const EVERY_DAY_TWICE_CRON = '0 30 8,20 * * *';
const TEST_CRON = '0/5 * * * * *';

@Injectable()
export class FeedDogCronService extends Subject<void> {
    @Cron(EVERY_DAY_TWICE_CRON, { timeZone: 'Europe/Moscow' })
    async handleDailyCron() {
        this.next();
    }
}
